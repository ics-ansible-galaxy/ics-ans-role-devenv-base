import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('physical_group')


def test_chronyd_running_and_enabled(host):
    service = host.service("chronyd")
    assert service.is_running
    assert service.is_enabled
