# ics-ans-role-devenv-base

Ansible role to install the devenv basic configuration.

## Requirements

- ansible >= 2.7
- molecule >= 2.6

## Role Variables

```yaml
devenv_base_yum_cron_update_cmd: security
devenv_base_yum_cron_apply_updates: yes
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-devenv-base
```

## License

BSD 2-clause
